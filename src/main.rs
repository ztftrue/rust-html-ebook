use std::fs::File;
use std::io::prelude::{Read, Write};
// let mut file = File::create("foo.txt")?;
// file.write_all(b"Hello, world!")?;
// Ok(())
fn main() -> std::io::Result<()> {
    let mut file = File::open("./file/foo.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let v: Vec<&str> = contents.split('\n').collect();
    let mut result = String::from("<?xml version='1.0' encoding='utf-8'?>\n");
    result.push_str("<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"zh\">\n");
    result.push_str("<head>\n");
    result.push_str("<title>");
    let stringp: &str = "<p>\n";
    let stringp2: &str = "</p>\n";
    // 如何获取索引
    let mut index = 0;
    let mut title = String::from("./file/");
    for i in v {
        if !i.is_empty() {
            println!("i = {}", i);
            // 竟然不需要括号
            if index == 0 {
                result.push_str(i);
                title.push_str(i);
                title.push_str(".html");
                result.push_str("</title>\n");
                result.push_str("</head>\n");
                result.push_str("<body>\n");
                result.push_str(" <h1>");
                result.push_str(i);
                result.push_str("</h1>\n");
            } else if index == 1 {
                result.push_str(" <span class=\"author\">");
                result.push_str(i);
                result.push_str("</span>\n");
            } else {
                result.push_str(stringp);
                result.push_str(i);
                result.push_str(stringp2);
            }
            index += 1;
        }
    }
    result.push_str("</body>\n");
    result.push_str("</html>");
    println!("{}", title);
    let mut file_result = File::create(title)?;
    file_result.write_all(result.as_bytes())?;
    Ok(())
}
