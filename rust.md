#

```sh
rustc main.rs
```

```rust
#![allow(unused)]
```

```rust
fn main() {}
``

```rust
  /* println!是一个Rust语言自带的宏， 这个宏的功能就是打印文本(结尾会换行) */
```

```rust
let rust = "Rust";
    println!("Hello, {}", rust);
    let a1 = 5;
    let a2: i32 = 5;

    //  assert_eq! 宏的作用是判断两个参数是不是相等的，但如果是两个不匹配的类型，就算字面值相等也会报警告。

    // assert_eq!(a1,a2);
    let b1: u32 = 5;

    let mut a: f64 = 1.0;
    let b = 2.0f32;

    //改变 a 的绑定
    a = 2.0;
    println!("{:?}", a);
    //重新绑定为不可变
    let a = a;
    //不能赋值
    // a = 3.0;
    let b2: isize = 0;
    //类型不匹配
    //assert_eq!(a, b);
```